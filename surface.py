import numpy as np
import skfuzzy.control as ctrl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # Required for 3D plotting

# Sparse universe makes calculations faster, without sacrifice accuracy.
# Only the critical points are included here; making it higher resolution is
# unnecessary.
universe = np.linspace(0, 10, 5)

# Create the three fuzzy variables - two inputs, one output
paz = ctrl.Antecedent(universe, 'paz')
orz = ctrl.Antecedent(universe, 'orz')
inz = ctrl.Antecedent(universe, 'inz')
output = ctrl.Consequent(universe, 'output')

# Here we use the convenience `automf` to populate the fuzzy variables with
# terms. The optional kwarg `names=` lets us specify the names of our Terms.
names = ['very low', 'low', 'medium', 'good', 'high']
paz.automf(names=names)
orz.automf(names=names)
inz.automf(names=names)
output.automf(names=names)

rule0 = ctrl.Rule(antecedent=((paz['very low'] & orz['very low']) |
                              (orz['very low'] & inz['very low']) |
                              (paz['very low'] & orz['very low']) |
                              (orz['very low'] & inz['low'])),
                  consequent=output['very low'], label='rule very low')

rule1 = ctrl.Rule(antecedent=((paz['very low'] & orz['very low']) |
                              (orz['very low'] & inz['medium']) |
                              (paz['very low'] & orz['very low']) |
                              (orz['very low'] & inz['good']) |
                              (paz['very low'] & orz['very low']) |
                              (orz['very low'] & inz['high']) |
                              (paz['very low'] & orz['low']) |
                              (orz['low'] & inz['low']) |
                              (paz['very low'] & orz['low']) |
                              (orz['low'] & inz['medium']) |
                              (paz['very low'] & orz['low']) |
                              (orz['low'] & inz['good']) |
                              (paz['very low'] & orz['medium']) |
                              (orz['medium'] & inz['medium']) |
                              (paz['low'] & orz['low']) |
                              (orz['low'] & inz['low']) |
                              (paz['low'] & orz['low']) |
                              (orz['low'] & inz['medium'])),
                  consequent=output['low'], label='rule low')

rule2 = ctrl.Rule(antecedent=((paz['very low'] & orz['low']) |
                              (orz['low'] & inz['high']) |
                              (paz['very low'] & orz['medium']) |
                              (orz['medium'] & inz['good']) |
                              (paz['very low'] & orz['medium']) |
                              (orz['medium'] & inz['high']) |
                              (paz['very low'] & orz['good']) |
                              (orz['good'] & inz['good']) |
                              (paz['very low'] & orz['good']) |
                              (orz['good'] & inz['good']) |
                              (paz['low'] & orz['low']) |
                              (orz['low'] & inz['good']) |
                              (paz['low'] & orz['low']) |
                              (orz['low'] & inz['high']) |
                              (paz['low'] & orz['medium']) |
                              (orz['medium'] & inz['medium']) |
                              (paz['low'] & orz['medium']) |
                              (orz['medium'] & inz['good']) |
                              (paz['low'] & orz['medium']) |
                              (orz['medium'] & inz['high']) |
                              (paz['low'] & orz['good']) |
                              (orz['good'] & inz['good']) |
                              (paz['medium'] & orz['medium']) |
                              (orz['medium'] & inz['medium']) |
                              (paz['medium'] & orz['medium']) |
                              (orz['medium'] & inz['good'])),
                  consequent=output['medium'], label='rule medium')

rule3 = ctrl.Rule(antecedent=((paz['very low'] & orz['high']) |
                              (orz['high'] & inz['high']) |
                              (paz['low'] & orz['good']) |
                              (orz['good'] & inz['high']) |
                              (paz['low'] & orz['high']) |
                              (orz['high'] & inz['high']) |
                              (paz['medium'] & orz['medium']) |
                              (orz['medium'] & inz['high']) |
                              (paz['medium'] & orz['good']) |
                              (orz['good'] & inz['good']) |
                              (paz['medium'] & orz['good']) |
                              (orz['good'] & inz['high']) |
                              (paz['medium'] & orz['high']) |
                              (orz['high'] & inz['high']) |
                              (paz['good'] & orz['good']) |
                              (orz['good'] & inz['good']) |
                              (paz['good'] & orz['good']) |
                              (orz['good'] & inz['high'])),
                  consequent=output['good'], label='rule good')

rule4 = ctrl.Rule(antecedent=((paz['good'] & orz['high']) |
                              (orz['high'] & inz['high']) |
                              (paz['high'] & orz['high']) |
                              (orz['high'] & inz['high'])),
                  consequent=output['high'], label='rule high')
system = ctrl.ControlSystem(rules=[rule0, rule1, rule2, rule3, rule4])

# Later we intend to run this system with a 21*21 set of inputs, so we allow
# that many plus one unique runs before results are flushed.
# Subsequent runs would return in 1/8 the time!
sim = ctrl.ControlSystemSimulation(system, flush_after_run=21 * 21 + 1)
# We can simulate at higher resolution with full accuracy
upsampled = np.linspace(0, 10, 21)
x, y = np.meshgrid(upsampled, upsampled)
z = np.zeros_like(x)

# Loop through the system 21*21 times to collect the control surface
for i in range(21):
    for j in range(21):
        sim.input['paz'] = x[i, j]
        sim.input['orz'] = y[i, j]
        sim.input['inz'] = y[i, j]
        sim.compute()
        z[i, j] = sim.output['output']

# Plot the result in pretty 3D with alpha blending


fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111, projection='3d')

surf = ax.plot_surface(x, y, z, rstride=1, cstride=1, cmap='viridis',
                       linewidth=0.4, antialiased=True)

cset = ax.contourf(x, y, z, zdir='z', offset=-2.5, cmap='viridis', alpha=0.5)
cset = ax.contourf(x, y, z, zdir='x', offset=3, cmap='viridis', alpha=0.5)
cset = ax.contourf(x, y, z, zdir='y', offset=3, cmap='viridis', alpha=0.5)

ax.view_init(30, 200)
plt.show()
