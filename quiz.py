from numpy import product
import copy
import sys


# Уровень угроз по определённой уязвимости
def th_count(pv, er):
    pv = float(pv)
    er = float(er)
    # return pv / 100 * er / 100
    return pv * er


# Уровень угроз по всем уязвимостям
def cth_count(th):
    diff = []
    for i in range(len(th)):
        diff.append(float(1 - th[i]))
    return '%.3f' % (1 - product(diff))


# Общий уровень угроз по ресурсу
def cthr_count(cth):
    diff = []
    for i in range(len(cth)):
        diff.append(1 - cth[i])
    return '%.3f' % (1 - product(diff))


# Риск по ресурсу
def risk(cthr):
    cthr = float(cthr)
    d = float(input('Введите критерий критичности (в рублях): \t'))
    return cthr * d


def make_test(topic):
    # for key in questions_paz.keys():
    #     for i in range(len(questions_paz[key])):
    #         print(key, questions_paz[key][i], sep=': ', end='\n')
    prob_vuln = {'paz': [],
                 'orz': [],
                 'inz': []}
    er = {'paz': [],
          'orz': [],
          'inz': []}

    questions_paz = {
        'Несанкционированный доступ к информации, хранящейся на сервере': [
            'Хранение данных на сервере в незашифрованном виде',
            'Отсутствие межсетевых экранов'],
        'Потеря информации из-за вирусов и шпионских программ': [
            'Отсутствие постоянно обновляемого антивирусного программного обеспечения, межсетевого экрана',
            'Использование нелицензионного программного обеспечения',
            'Отсутствие ограничения доступа пользователей к внешней сети'],
        'Несанкционированный доступ к информации, хранящейся на автоматизированном рабочем месте': [
            'Недостаточноть системы аутентификации пользователей',
            'Отсутствие средств защиты от несанкционированного доступа по сети (VPN и др.)']}

    questions_orz = {
        'Физический доступ нарушителя к документам': [
            'Недостатки в организации контрольно-пропускного режима на предприятии',
            'Отсутствие видеонаблюдения'],
        'Разглашение конфиденциальной информации, используемой в документах,'
        'вынос документов за пределы контрольной зоны': [
            'Отсутствие соглашения о неразглашении конфиденциальной информации',
            'Нечёткое распределение ответственности за документы между сотрудниками предприятия'],
        'Несанкционированное копирование, печать и размножение конфиденциальных документов': [
            'Нечёткая организация конфиденциального документооборота',
            'Неконтролируемый доступ сотрудников к копировальной множительной технике']}

    questions_inz = {
        'Съём информации за счёт побочного электромагнитного излучения': [
            'Отсутствие генераторов зашумления, экранирования',
            'Превышение уровня опасного сигнала за пределами контрольной зоны'],
        'Съём информации с телефонной линии': [
            'Отсутствие устройств контроля напряжения телефонной линии',
            'Не проводятся специальные обследования и специальные проверки при'
            'установке нового оборудования, а также при проведении совещаний'],
        'Пожар': [
            'Уязвимости в системе противопожарной сигнализации, истечение срока эксплуатации огнетушителей',
            'Отсутствие несгораемого сейфа']}

    # topic = input('Пожалуйста, выберите категорию:\n '
    #               '1 - Программно-аппаратная защита\n '
    #               '2 - Организационная защита\n '
    #               '3 - Инженерно-техническая защита\n'
    #               'Для выхода введите "выход" или "exit" \n')
    if topic == '1':
        topic = 'paz'
        for key in questions_paz.keys():
            print('Угроза: ', key.lower())
            for i in range(len(questions_paz[key])):
                print('Уязвимость: ', questions_paz[key][i].lower())
                answer_vuln = input('Введите вероятность реализации данной '
                                    'угрозы через уязвимость в течение года (в %): \t')
                prob_vuln['paz'].append(answer_vuln)
                answer_er = input('Введите критичность реализации угрозы (в %): \t')
                er['paz'].append(answer_er)
        return list(questions_paz.keys()), prob_vuln[topic], er[topic], questions_paz
    elif topic == '2':
        topic = 'orz'
        for key in questions_orz.keys():
            print('Угроза: ', key.lower())
            for i in range(len(questions_orz[key])):
                print('Уязвимость: ', questions_orz[key][i].lower())
                answer_vuln = input('Введите вероятность реализации данной '
                                    'угрозы через уязвимость в течение года (в %): \t')
                prob_vuln['orz'].append(answer_vuln)
                answer_er = input('Введите критичность реализации угрозы (в %): \t')
                er['orz'].append(answer_er)
        return list(questions_orz.keys()), prob_vuln[topic], er[topic], questions_orz
    elif topic == '3':
        topic = 'inz'
        for key in questions_inz.keys():
            print('Угроза: ', key.lower())
            for i in range(len(questions_inz[key])):
                print('Уязвимость: ', questions_inz[key][i].lower())
                answer_vuln = input('Введите вероятность реализации данной '
                                    'угрозы через уязвимость в течение года (в %): \t')
                prob_vuln['inz'].append(answer_vuln)
                answer_er = input('Введите критичность реализации угрозы (в %): \t')
                er['inz'].append(answer_er)
        return list(questions_inz.keys()), prob_vuln[topic], er[topic], questions_inz
    elif topic == 'e' or 'в':
        sys.exit()


def start_quiz(topic):
    # for i in range(3):
    test = make_test(topic)
    # print(test[0], test[1], test[2], sep='\n')

    th_test = list()
    keys = []

    for i in range(len(test[0])):
        keys.append(test[0][i])

    # print(len(test[3][keys[1]]))

    test1_twin = copy.deepcopy(test[1])
    test2_twin = copy.deepcopy(test[2])
    cth_list = []

    for i in range(len(test[3])):
        if test2_twin and test1_twin is not None:
            for j in range(len(test[3][keys[i]])):
                th_test.append(float(th_count(test1_twin.pop(0), test2_twin.pop(0))))
            cth_list.append(float(cth_count(th_test[0:j + 1])))
            print('Уровень угрозы по уязвимости', keys[i].lower(), cth_list[i], sep=': ')
            th_test.clear()

    # print('Общий уровень угроз по ресурсу', cthr_count(cth_list), sep=': ')
    vuln_level = float(cthr_count(cth_list)) * 10
    # print('Риск по ресурсу равен: ', risk(cthr_count(cth_list)), 'рублей')
    return vuln_level, risk(cthr_count(cth_list))
