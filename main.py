import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
import matplotlib.pyplot as plt
import quiz

# оцениваем ПаЗ
# в диапазоне (0, 10) с шагом 1.
# Эта оценка является антедецентом.
paz = ctrl.Antecedent(np.arange(0, 11, 1), 'Программно-аппаратная защита')

# оцениваем ОргЗ
# в диапазоне (0, 10) с шагом 1.
# Эта оценка является антедецентом.
orz = ctrl.Antecedent(np.arange(0, 11, 1), 'Организационная защита')

# оцениваем ИнжЗ
# в диапазоне (0, 10) с шагом 1.
# Эта оценка является антедецентом.
inz = ctrl.Antecedent(np.arange(0, 11, 1), 'Инженерно-техническая защита')

# оцениваем безопасность (риски)
# в диапазоне (0, 100) с шагом 1.
# Эта оценка является консеквентом.
security = ctrl.Consequent(np.arange(0, 101, 1), 'Security')

# автоматически определяем функции принадлежности
# paz.automf(5)
# orz.automf(5)
# inz.automf(5)

'''Значения, подобранные вручную'''
# paz['very low'] = fuzz.trimf(paz.universe, [0, 0, 2.5])
# paz['low'] = fuzz.trimf(paz.universe, [0, 2.5, 5])
# paz['medium'] = fuzz.trimf(paz.universe, [2.5, 5, 7.5])
# paz['good'] = fuzz.trimf(paz.universe, [5, 7.5, 10])
# paz['high'] = fuzz.trimf(paz.universe, [7.5, 10, 10])
#
# orz['very low'] = fuzz.trimf(orz.universe, [0, 0, 2.5])
# orz['low'] = fuzz.trimf(orz.universe, [0, 2.5, 5])
# orz['medium'] = fuzz.trimf(orz.universe, [2.5, 5, 7.5])
# orz['good'] = fuzz.trimf(orz.universe, [5, 7.5, 10])
# orz['high'] = fuzz.trimf(orz.universe, [7.5, 10, 10])
#
# inz['very low'] = fuzz.trimf(inz.universe, [0, 0, 2.5])
# inz['low'] = fuzz.trimf(inz.universe, [0, 2.5, 5])
# inz['medium'] = fuzz.trimf(inz.universe, [2.5, 5, 7.5])
# inz['good'] = fuzz.trimf(inz.universe, [5, 7.5, 10])
# inz['high'] = fuzz.trimf(inz.universe, [7.5, 10, 10])

'''Значения, взятые в соответствии с функцией желательности Харрингтона'''
paz['very low'] = fuzz.trimf(paz.universe, [0, 0, 2])
paz['low'] = fuzz.trimf(paz.universe, [0, 2, 3.7])
paz['medium'] = fuzz.trimf(paz.universe, [2, 3.7, 6.3])
paz['good'] = fuzz.trimf(paz.universe, [3.7, 6.3, 8])
paz['high'] = fuzz.trimf(paz.universe, [6.3, 8, 10])

orz['very low'] = fuzz.trimf(orz.universe, [0, 0, 2])
orz['low'] = fuzz.trimf(orz.universe, [0, 2, 3.7])
orz['medium'] = fuzz.trimf(orz.universe, [2, 3.7, 6.3])
orz['good'] = fuzz.trimf(orz.universe, [3.7, 6.3, 8])
orz['high'] = fuzz.trimf(orz.universe, [6.3, 8, 10])

inz['very low'] = fuzz.trimf(inz.universe, [0, 0, 2])
inz['low'] = fuzz.trimf(inz.universe, [0, 2, 3.7])
inz['medium'] = fuzz.trimf(inz.universe, [2, 3.7, 6.3])
inz['good'] = fuzz.trimf(inz.universe, [3.7, 6.3, 8])
inz['high'] = fuzz.trimf(inz.universe, [6.3, 8, 10])

# определяем пользовательские функции принадлежности
security['very low'] = fuzz.trimf(security.universe, [0, 0, 20])
security['low'] = fuzz.trimf(security.universe, [0, 20, 40])
security['medium'] = fuzz.trimf(security.universe, [20, 40, 60])
security['good'] = fuzz.trimf(security.universe, [40, 60, 80])
security['high'] = fuzz.trimf(security.universe, [60, 80, 100])

rule_1 = ctrl.Rule(paz['very low'] & orz['very low'] & inz['very low'],
                   security['very low'])
rule_2 = ctrl.Rule(paz['very low'] & orz['very low'] & inz['low'],
                   security['very low'])
rule_3 = ctrl.Rule(paz['very low'] & orz['very low'] & inz['medium'],
                   security['low'])
rule_4 = ctrl.Rule(paz['very low'] & orz['very low'] & inz['good'],
                   security['low'])
rule_5 = ctrl.Rule(paz['very low'] & orz['very low'] & inz['high'],
                   security['low'])

rule_6 = ctrl.Rule(paz['very low'] & orz['low'] & inz['very low'],
                   security['very low'])
rule_7 = ctrl.Rule(paz['very low'] & orz['low'] & inz['low'],
                   security['low'])
rule_8 = ctrl.Rule(paz['very low'] & orz['low'] & inz['medium'],
                   security['low'])
rule_9 = ctrl.Rule(paz['very low'] & orz['low'] & inz['good'],
                   security['low'])
rule_10 = ctrl.Rule(paz['very low'] & orz['low'] & inz['high'],
                    security['medium'])

rule_11 = ctrl.Rule(paz['very low'] & orz['medium'] & inz['very low'],
                    security['very low'])
rule_12 = ctrl.Rule(paz['very low'] & orz['medium'] & inz['low'],
                    security['low'])
rule_13 = ctrl.Rule(paz['very low'] & orz['medium'] & inz['medium'],
                    security['low'])
rule_14 = ctrl.Rule(paz['very low'] & orz['medium'] & inz['good'],
                    security['medium'])
rule_15 = ctrl.Rule(paz['very low'] & orz['medium'] & inz['high'],
                    security['medium'])

rule_16 = ctrl.Rule(paz['very low'] & orz['good'] & inz['very low'],
                    security['low'])
rule_17 = ctrl.Rule(paz['very low'] & orz['good'] & inz['low'],
                    security['low'])
rule_18 = ctrl.Rule(paz['very low'] & orz['good'] & inz['medium'],
                    security['medium'])
rule_19 = ctrl.Rule(paz['very low'] & orz['good'] & inz['good'],
                    security['medium'])
rule_20 = ctrl.Rule(paz['very low'] & orz['good'] & inz['high'],
                    security['medium'])

rule_21 = ctrl.Rule(paz['very low'] & orz['high'] & inz['very low'],
                    security['low'])
rule_22 = ctrl.Rule(paz['very low'] & orz['high'] & inz['low'],
                    security['medium'])
rule_23 = ctrl.Rule(paz['very low'] & orz['high'] & inz['medium'],
                    security['medium'])
rule_24 = ctrl.Rule(paz['very low'] & orz['high'] & inz['good'],
                    security['medium'])
rule_25 = ctrl.Rule(paz['very low'] & orz['high'] & inz['high'],
                    security['good'])

rule_26 = ctrl.Rule(paz['low'] & orz['very low'] & inz['very low'],
                    security['very low'])
rule_27 = ctrl.Rule(paz['low'] & orz['very low'] & inz['low'],
                    security['low'])
rule_28 = ctrl.Rule(paz['low'] & orz['very low'] & inz['medium'],
                    security['low'])
rule_29 = ctrl.Rule(paz['low'] & orz['very low'] & inz['good'],
                    security['low'])
rule_30 = ctrl.Rule(paz['low'] & orz['very low'] & inz['high'],
                    security['medium'])

rule_31 = ctrl.Rule(paz['low'] & orz['low'] & inz['very low'],
                    security['low'])
rule_32 = ctrl.Rule(paz['low'] & orz['low'] & inz['low'],
                    security['low'])
rule_33 = ctrl.Rule(paz['low'] & orz['low'] & inz['medium'],
                    security['low'])
rule_34 = ctrl.Rule(paz['low'] & orz['low'] & inz['good'],
                    security['medium'])
rule_35 = ctrl.Rule(paz['low'] & orz['low'] & inz['high'],
                    security['medium'])

rule_36 = ctrl.Rule(paz['low'] & orz['medium'] & inz['very low'],
                    security['low'])
rule_37 = ctrl.Rule(paz['low'] & orz['medium'] & inz['low'],
                    security['low'])
rule_38 = ctrl.Rule(paz['low'] & orz['medium'] & inz['medium'],
                    security['medium'])
rule_39 = ctrl.Rule(paz['low'] & orz['medium'] & inz['good'],
                    security['medium'])
rule_40 = ctrl.Rule(paz['low'] & orz['medium'] & inz['high'],
                    security['medium'])

rule_41 = ctrl.Rule(paz['low'] & orz['good'] & inz['very low'],
                    security['low'])
rule_42 = ctrl.Rule(paz['low'] & orz['good'] & inz['low'],
                    security['medium'])
rule_43 = ctrl.Rule(paz['low'] & orz['good'] & inz['medium'],
                    security['medium'])
rule_44 = ctrl.Rule(paz['low'] & orz['good'] & inz['good'],
                    security['medium'])
rule_45 = ctrl.Rule(paz['low'] & orz['good'] & inz['high'],
                    security['good'])

rule_46 = ctrl.Rule(paz['low'] & orz['high'] & inz['very low'],
                    security['medium'])
rule_47 = ctrl.Rule(paz['low'] & orz['high'] & inz['low'],
                    security['medium'])
rule_48 = ctrl.Rule(paz['low'] & orz['high'] & inz['medium'],
                    security['medium'])
rule_49 = ctrl.Rule(paz['low'] & orz['high'] & inz['good'],
                    security['good'])
rule_50 = ctrl.Rule(paz['low'] & orz['high'] & inz['high'],
                    security['good'])

rule_51 = ctrl.Rule(paz['medium'] & orz['very low'] & inz['very low'],
                    security['low'])
rule_52 = ctrl.Rule(paz['medium'] & orz['very low'] & inz['low'],
                    security['low'])
rule_53 = ctrl.Rule(paz['medium'] & orz['very low'] & inz['medium'],
                    security['low'])
rule_54 = ctrl.Rule(paz['medium'] & orz['very low'] & inz['good'],
                    security['medium'])
rule_55 = ctrl.Rule(paz['medium'] & orz['very low'] & inz['high'],
                    security['medium'])

rule_56 = ctrl.Rule(paz['medium'] & orz['low'] & inz['very low'],
                    security['low'])
rule_57 = ctrl.Rule(paz['medium'] & orz['low'] & inz['low'],
                    security['low'])
rule_58 = ctrl.Rule(paz['medium'] & orz['low'] & inz['medium'],
                    security['medium'])
rule_59 = ctrl.Rule(paz['medium'] & orz['low'] & inz['good'],
                    security['medium'])
rule_60 = ctrl.Rule(paz['medium'] & orz['low'] & inz['high'],
                    security['medium'])

rule_61 = ctrl.Rule(paz['medium'] & orz['medium'] & inz['very low'],
                    security['low'])
rule_62 = ctrl.Rule(paz['medium'] & orz['medium'] & inz['low'],
                    security['medium'])
rule_63 = ctrl.Rule(paz['medium'] & orz['medium'] & inz['medium'],
                    security['medium'])
rule_64 = ctrl.Rule(paz['medium'] & orz['medium'] & inz['good'],
                    security['medium'])
rule_65 = ctrl.Rule(paz['medium'] & orz['medium'] & inz['high'],
                    security['good'])

rule_66 = ctrl.Rule(paz['medium'] & orz['good'] & inz['very low'],
                    security['medium'])
rule_67 = ctrl.Rule(paz['medium'] & orz['good'] & inz['low'],
                    security['medium'])
rule_68 = ctrl.Rule(paz['medium'] & orz['good'] & inz['medium'],
                    security['medium'])
rule_69 = ctrl.Rule(paz['medium'] & orz['good'] & inz['good'],
                    security['good'])
rule_70 = ctrl.Rule(paz['medium'] & orz['good'] & inz['high'],
                    security['good'])

rule_71 = ctrl.Rule(paz['medium'] & orz['high'] & inz['very low'],
                    security['medium'])
rule_72 = ctrl.Rule(paz['medium'] & orz['high'] & inz['low'],
                    security['medium'])
rule_73 = ctrl.Rule(paz['medium'] & orz['high'] & inz['medium'],
                    security['good'])
rule_74 = ctrl.Rule(paz['medium'] & orz['high'] & inz['good'],
                    security['good'])
rule_75 = ctrl.Rule(paz['medium'] & orz['high'] & inz['high'],
                    security['good'])

rule_76 = ctrl.Rule(paz['good'] & orz['very low'] & inz['very low'],
                    security['low'])
rule_77 = ctrl.Rule(paz['good'] & orz['very low'] & inz['low'],
                    security['low'])
rule_78 = ctrl.Rule(paz['good'] & orz['very low'] & inz['medium'],
                    security['medium'])
rule_79 = ctrl.Rule(paz['good'] & orz['very low'] & inz['good'],
                    security['medium'])
rule_80 = ctrl.Rule(paz['good'] & orz['very low'] & inz['high'],
                    security['medium'])

rule_81 = ctrl.Rule(paz['good'] & orz['low'] & inz['very low'],
                    security['low'])
rule_82 = ctrl.Rule(paz['good'] & orz['low'] & inz['low'],
                    security['medium'])
rule_83 = ctrl.Rule(paz['good'] & orz['low'] & inz['medium'],
                    security['medium'])
rule_84 = ctrl.Rule(paz['good'] & orz['low'] & inz['good'],
                    security['medium'])
rule_85 = ctrl.Rule(paz['good'] & orz['low'] & inz['high'],
                    security['good'])

rule_86 = ctrl.Rule(paz['good'] & orz['medium'] & inz['very low'],
                    security['medium'])
rule_87 = ctrl.Rule(paz['good'] & orz['medium'] & inz['low'],
                    security['medium'])
rule_88 = ctrl.Rule(paz['good'] & orz['medium'] & inz['medium'],
                    security['medium'])
rule_89 = ctrl.Rule(paz['good'] & orz['medium'] & inz['good'],
                    security['good'])
rule_90 = ctrl.Rule(paz['good'] & orz['medium'] & inz['high'],
                    security['good'])

rule_91 = ctrl.Rule(paz['good'] & orz['good'] & inz['very low'],
                    security['medium'])
rule_92 = ctrl.Rule(paz['good'] & orz['good'] & inz['low'],
                    security['medium'])
rule_93 = ctrl.Rule(paz['good'] & orz['good'] & inz['medium'],
                    security['good'])
rule_94 = ctrl.Rule(paz['good'] & orz['good'] & inz['good'],
                    security['good'])
rule_95 = ctrl.Rule(paz['good'] & orz['good'] & inz['high'],
                    security['good'])

rule_96 = ctrl.Rule(paz['good'] & orz['high'] & inz['very low'],
                    security['medium'])
rule_97 = ctrl.Rule(paz['good'] & orz['high'] & inz['low'],
                    security['good'])
rule_98 = ctrl.Rule(paz['good'] & orz['high'] & inz['medium'],
                    security['good'])
rule_99 = ctrl.Rule(paz['good'] & orz['high'] & inz['good'],
                    security['good'])
rule_100 = ctrl.Rule(paz['good'] & orz['high'] & inz['high'],
                     security['high'])

rule_101 = ctrl.Rule(paz['high'] & orz['very low'] & inz['very low'],
                     security['low'])
rule_102 = ctrl.Rule(paz['high'] & orz['very low'] & inz['low'],
                     security['medium'])
rule_103 = ctrl.Rule(paz['high'] & orz['very low'] & inz['medium'],
                     security['medium'])
rule_104 = ctrl.Rule(paz['high'] & orz['very low'] & inz['good'],
                     security['medium'])
rule_105 = ctrl.Rule(paz['high'] & orz['very low'] & inz['high'],
                     security['good'])

rule_106 = ctrl.Rule(paz['high'] & orz['low'] & inz['very low'],
                     security['medium'])
rule_107 = ctrl.Rule(paz['high'] & orz['low'] & inz['low'],
                     security['medium'])
rule_108 = ctrl.Rule(paz['high'] & orz['low'] & inz['medium'],
                     security['medium'])
rule_109 = ctrl.Rule(paz['high'] & orz['low'] & inz['good'],
                     security['good'])
rule_110 = ctrl.Rule(paz['high'] & orz['low'] & inz['high'],
                     security['good'])

rule_111 = ctrl.Rule(paz['high'] & orz['medium'] & inz['very low'],
                     security['medium'])
rule_112 = ctrl.Rule(paz['high'] & orz['medium'] & inz['low'],
                     security['medium'])
rule_113 = ctrl.Rule(paz['high'] & orz['medium'] & inz['medium'],
                     security['good'])
rule_114 = ctrl.Rule(paz['high'] & orz['medium'] & inz['good'],
                     security['good'])
rule_115 = ctrl.Rule(paz['high'] & orz['medium'] & inz['high'],
                     security['good'])

rule_116 = ctrl.Rule(paz['high'] & orz['good'] & inz['very low'],
                     security['medium'])
rule_117 = ctrl.Rule(paz['high'] & orz['good'] & inz['low'],
                     security['good'])
rule_118 = ctrl.Rule(paz['high'] & orz['good'] & inz['medium'],
                     security['good'])
rule_119 = ctrl.Rule(paz['high'] & orz['good'] & inz['good'],
                     security['good'])
rule_120 = ctrl.Rule(paz['high'] & orz['good'] & inz['high'],
                     security['high'])

rule_121 = ctrl.Rule(paz['high'] & orz['high'] & inz['very low'],
                     security['good'])
rule_122 = ctrl.Rule(paz['high'] & orz['high'] & inz['low'],
                     security['good'])
rule_123 = ctrl.Rule(paz['high'] & orz['high'] & inz['medium'],
                     security['good'])
rule_124 = ctrl.Rule(paz['high'] & orz['high'] & inz['good'],
                     security['high'])
rule_125 = ctrl.Rule(paz['high'] & orz['high'] & inz['high'],
                     security['high'])

rule_126 = ctrl.Rule(paz['very low'] & orz['very low'],
                     security['very low'])
rule_127 = ctrl.Rule(paz['very low'] & orz['low'],
                     security['low'])
rule_128 = ctrl.Rule(paz['very low'] & orz['medium'],
                     security['low'])
rule_129 = ctrl.Rule(paz['very low'] & orz['good'],
                     security['medium'])
rule_130 = ctrl.Rule(paz['very low'] & orz['high'],
                     security['medium'])

rule_131 = ctrl.Rule(paz['low'] & orz['very low'],
                     security['low'])
rule_132 = ctrl.Rule(paz['low'] & orz['low'],
                     security['low'])
rule_133 = ctrl.Rule(paz['low'] & orz['medium'],
                     security['medium'])
rule_134 = ctrl.Rule(paz['low'] & orz['good'],
                     security['medium'])
rule_135 = ctrl.Rule(paz['low'] & orz['high'],
                     security['good'])

rule_136 = ctrl.Rule(paz['medium'] & orz['very low'],
                     security['low'])
rule_137 = ctrl.Rule(paz['medium'] & orz['low'],
                     security['medium'])
rule_138 = ctrl.Rule(paz['medium'] & orz['medium'],
                     security['medium'])
rule_139 = ctrl.Rule(paz['medium'] & orz['good'],
                     security['good'])
rule_140 = ctrl.Rule(paz['medium'] & orz['high'],
                     security['good'])

rule_141 = ctrl.Rule(paz['good'] & orz['very low'],
                     security['medium'])
rule_142 = ctrl.Rule(paz['good'] & orz['low'],
                     security['medium'])
rule_143 = ctrl.Rule(paz['good'] & orz['medium'],
                     security['good'])
rule_144 = ctrl.Rule(paz['good'] & orz['good'],
                     security['good'])
rule_145 = ctrl.Rule(paz['good'] & orz['high'],
                     security['high'])

rule_146 = ctrl.Rule(paz['high'] & orz['very low'],
                     security['medium'])
rule_147 = ctrl.Rule(paz['high'] & orz['low'],
                     security['good'])
rule_148 = ctrl.Rule(paz['high'] & orz['medium'],
                     security['good'])
rule_149 = ctrl.Rule(paz['high'] & orz['good'],
                     security['high'])
rule_150 = ctrl.Rule(paz['high'] & orz['high'],
                     security['high'])

rule_151 = ctrl.Rule(paz['very low'] & inz['very low'],
                     security['very low'])
rule_152 = ctrl.Rule(paz['very low'] & inz['low'],
                     security['low'])
rule_153 = ctrl.Rule(paz['very low'] & inz['medium'],
                     security['low'])
rule_154 = ctrl.Rule(paz['very low'] & inz['good'],
                     security['medium'])
rule_155 = ctrl.Rule(paz['very low'] & inz['high'],
                     security['medium'])

rule_156 = ctrl.Rule(paz['low'] & inz['very low'],
                     security['low'])
rule_157 = ctrl.Rule(paz['low'] & inz['low'],
                     security['low'])
rule_158 = ctrl.Rule(paz['low'] & inz['medium'],
                     security['medium'])
rule_159 = ctrl.Rule(paz['low'] & inz['good'],
                     security['medium'])
rule_160 = ctrl.Rule(paz['low'] & inz['high'],
                     security['good'])

rule_161 = ctrl.Rule(paz['medium'] & inz['very low'],
                     security['low'])
rule_162 = ctrl.Rule(paz['medium'] & inz['low'],
                     security['medium'])
rule_163 = ctrl.Rule(paz['medium'] & inz['medium'],
                     security['medium'])
rule_164 = ctrl.Rule(paz['medium'] & inz['good'],
                     security['good'])
rule_165 = ctrl.Rule(paz['medium'] & inz['high'],
                     security['good'])

rule_166 = ctrl.Rule(paz['good'] & inz['very low'],
                     security['medium'])
rule_167 = ctrl.Rule(paz['good'] & inz['low'],
                     security['medium'])
rule_168 = ctrl.Rule(paz['good'] & inz['medium'],
                     security['good'])
rule_169 = ctrl.Rule(paz['good'] & inz['good'],
                     security['good'])
rule_170 = ctrl.Rule(paz['good'] & inz['high'],
                     security['high'])

rule_171 = ctrl.Rule(paz['high'] & inz['very low'],
                     security['medium'])
rule_172 = ctrl.Rule(paz['high'] & inz['low'],
                     security['good'])
rule_173 = ctrl.Rule(paz['high'] & inz['medium'],
                     security['good'])
rule_174 = ctrl.Rule(paz['high'] & inz['good'],
                     security['high'])
rule_175 = ctrl.Rule(paz['high'] & inz['high'],
                     security['high'])

rule_176 = ctrl.Rule(orz['very low'] & inz['very low'],
                     security['very low'])
rule_177 = ctrl.Rule(orz['very low'] & inz['low'],
                     security['low'])
rule_178 = ctrl.Rule(orz['very low'] & inz['medium'],
                     security['low'])
rule_179 = ctrl.Rule(orz['very low'] & inz['good'],
                     security['medium'])
rule_180 = ctrl.Rule(orz['very low'] & inz['high'],
                     security['medium'])

rule_181 = ctrl.Rule(orz['low'] & inz['very low'],
                     security['low'])
rule_182 = ctrl.Rule(orz['low'] & inz['low'],
                     security['low'])
rule_183 = ctrl.Rule(orz['low'] & inz['medium'],
                     security['medium'])
rule_184 = ctrl.Rule(orz['low'] & inz['good'],
                     security['medium'])
rule_185 = ctrl.Rule(orz['low'] & inz['high'],
                     security['good'])

rule_186 = ctrl.Rule(orz['medium'] & inz['very low'],
                     security['low'])
rule_187 = ctrl.Rule(orz['medium'] & inz['low'],
                     security['medium'])
rule_188 = ctrl.Rule(orz['medium'] & inz['medium'],
                     security['medium'])
rule_189 = ctrl.Rule(orz['medium'] & inz['good'],
                     security['good'])
rule_190 = ctrl.Rule(orz['medium'] & inz['high'],
                     security['good'])

rule_191 = ctrl.Rule(orz['good'] & inz['very low'],
                     security['medium'])
rule_192 = ctrl.Rule(orz['good'] & inz['low'],
                     security['medium'])
rule_193 = ctrl.Rule(orz['good'] & inz['medium'],
                     security['good'])
rule_194 = ctrl.Rule(orz['good'] & inz['good'],
                     security['good'])
rule_195 = ctrl.Rule(orz['good'] & inz['high'],
                     security['high'])

rule_196 = ctrl.Rule(orz['high'] & inz['very low'],
                     security['medium'])
rule_197 = ctrl.Rule(orz['high'] & inz['low'],
                     security['good'])
rule_198 = ctrl.Rule(orz['high'] & inz['medium'],
                     security['good'])
rule_199 = ctrl.Rule(orz['high'] & inz['good'],
                     security['high'])
rule_200 = ctrl.Rule(orz['high'] & inz['high'],
                     security['high'])

rule_201 = ctrl.Rule(inz['very low'], security['very low'])
rule_202 = ctrl.Rule(inz['low'], security['low'])
rule_203 = ctrl.Rule(inz['medium'], security['medium'])
rule_204 = ctrl.Rule(inz['good'], security['good'])
rule_205 = ctrl.Rule(inz['high'], security['high'])

rule_206 = ctrl.Rule(orz['very low'], security['very low'])
rule_207 = ctrl.Rule(orz['low'], security['low'])
rule_208 = ctrl.Rule(orz['medium'], security['medium'])
rule_209 = ctrl.Rule(orz['good'], security['good'])
rule_210 = ctrl.Rule(orz['high'], security['high'])

rule_211 = ctrl.Rule(paz['very low'], security['very low'])
rule_212 = ctrl.Rule(paz['low'], security['low'])
rule_213 = ctrl.Rule(paz['medium'], security['medium'])
rule_214 = ctrl.Rule(paz['good'], security['good'])
rule_215 = ctrl.Rule(paz['high'], security['high'])

topic_list = []
rules = []
paz_rating, orz_rating, inz_rating = None, None, None
# введём оценки (в диапазоне от 0 до 10!) для 3 базовых угроз
while True:
    topic = input('Пожалуйста, выберите категорию:\n '
                  '1 - Программно-аппаратная защита\n '
                  '2 - Организационная защита\n '
                  '3 - Инженерно-техническая защита\n'
                  'Для выхода введите "выход" или "exit" \n')
    my_file = open("report.txt", "w", encoding='utf-8')
    my_file.write("Оценка начата.\n")
    my_file.close()
    if topic == '1':
        state = quiz.start_quiz(topic)
        paz_rating = state[0]
        paz_risk = state[1]
    elif topic == '2':
        state = quiz.start_quiz(topic)
        orz_rating = state[0]
        orz_risk = state[1]
    elif topic == '3':
        state = quiz.start_quiz(topic)
        inz_rating = state[0]
        inz_risk = state[1]
    elif 'в' or 'e' in topic:
        break
    topic_list.append(topic)

if '1' and '2' and '3' in topic_list:
    rules = [rule_1, rule_2, rule_3, rule_4, rule_5,
             rule_6, rule_7, rule_8, rule_9, rule_10,
             rule_11, rule_12, rule_13, rule_14, rule_15,
             rule_16, rule_17, rule_18, rule_19, rule_20,
             rule_21, rule_22, rule_23, rule_24, rule_25,
             rule_26, rule_27, rule_28, rule_29, rule_30,
             rule_31, rule_32, rule_33, rule_34, rule_35,
             rule_36, rule_37, rule_38, rule_39, rule_40,
             rule_41, rule_42, rule_43, rule_44, rule_45,
             rule_46, rule_47, rule_48, rule_49, rule_50,
             rule_51, rule_52, rule_53, rule_54, rule_55,
             rule_56, rule_57, rule_58, rule_59, rule_60,
             rule_61, rule_62, rule_63, rule_64, rule_65,
             rule_66, rule_67, rule_68, rule_69, rule_70,
             rule_71, rule_72, rule_73, rule_74, rule_75,
             rule_76, rule_77, rule_78, rule_79, rule_80,
             rule_81, rule_82, rule_83, rule_84, rule_85,
             rule_86, rule_87, rule_88, rule_89, rule_90,
             rule_91, rule_92, rule_93, rule_94, rule_95,
             rule_96, rule_97, rule_98, rule_99, rule_100,
             rule_101, rule_102, rule_103, rule_104, rule_105,
             rule_106, rule_107, rule_108, rule_109, rule_110,
             rule_111, rule_112, rule_113, rule_114, rule_115,
             rule_116, rule_117, rule_118, rule_119, rule_120,
             rule_121, rule_122, rule_123, rule_124, rule_125]
    my_file = open("report.txt", "a", encoding='utf-8')
    my_file.write("Оценены все категории.\n")
    my_file.close()
elif '1' not in topic_list:
    # print('Не оценена программно-аппаратная защита.')
    inz_rating = 0
    rules = [rule_176, rule_177, rule_178, rule_179, rule_180,
             rule_181, rule_182, rule_183, rule_184, rule_185,
             rule_186, rule_187, rule_188, rule_189, rule_190,
             rule_191, rule_192, rule_193, rule_194, rule_195,
             rule_196, rule_197, rule_198, rule_199, rule_200]
    my_file = open("report.txt", "a", encoding='utf-8')
    my_file.write("Не оценена программно-аппаратная защита.\n")
    my_file.close()
elif '2' not in topic_list:
    # print('Не оценена организационная защита.')
    orz_rating = 0
    rules = [rule_151, rule_152, rule_153, rule_154, rule_155,
             rule_156, rule_157, rule_158, rule_159, rule_160,
             rule_161, rule_162, rule_163, rule_164, rule_165,
             rule_166, rule_167, rule_168, rule_169, rule_170,
             rule_171, rule_172, rule_173, rule_174, rule_175]
    my_file = open("report.txt", "a", encoding='utf-8')
    my_file.write("Не оценена организационная защита.\n")
    my_file.close()
elif '3' not in topic_list:
    # print('Не оценена инженерно-техническая защита.')
    inz_rating = 0
    rules = [rule_126, rule_127, rule_128, rule_129, rule_130,
             rule_131, rule_132, rule_133, rule_134, rule_135,
             rule_136, rule_137, rule_138, rule_139, rule_140,
             rule_141, rule_142, rule_143, rule_144, rule_145,
             rule_146, rule_147, rule_148, rule_149, rule_150]
    my_file = open("report.txt", "a", encoding='utf-8')
    my_file.write("Не оценена инженерно-техническая защита.\n")
    my_file.close()
elif '1' and '2' not in topic_list:
    # print('Не оценена программно-аппаратная защита и организационная защита.')
    paz_rating = 0
    orz_rating = 0
    rules = [rule_201, rule_202, rule_203, rule_204, rule_205]
    my_file = open("report.txt", "a", encoding='utf-8')
    my_file.write("Не оценена программно-аппаратная защита и организационная защита.\n")
    my_file.close()
elif '1' and '3' not in topic_list:
    # print('Не оценена программно-аппаратная защита и инженерно-техническая защита.')
    paz_rating = 0
    inz_rating = 0
    rules = [rule_206, rule_207, rule_208, rule_209, rule_210]
    my_file = open("report.txt", "a", encoding='utf-8')
    my_file.write("Не оценена программно-аппаратная защита и инженерно-техническая защита.\n")
    my_file.close()
elif '2' and '3' not in topic_list:
    # print('Не оценена организационная защита и инженерно-техническая защита.')
    orz_rating = 0
    inz_rating = 0
    rules = [rule_211, rule_212, rule_213, rule_214, rule_215]
    my_file = open("report.txt", "a", encoding='utf-8')
    my_file.write("Не оценена организационная защита и инженерно-техническая защита.\n")
    my_file.close()

security_ctrl = ctrl.ControlSystem(rules)
security_is = ctrl.ControlSystemSimulation(security_ctrl)

security_is.input['Программно-аппаратная защита'] = paz_rating
security_is.input['Организационная защита'] = orz_rating
security_is.input['Инженерно-техническая защита'] = inz_rating

security_is.compute()

if 0 < security_is.output['Security'] < 20:
    print('Очень низкий уровень безопасности')
elif 20 < security_is.output['Security'] < 40:
    print('Низкий уровень безопасности')
elif 40 < security_is.output['Security'] < 60:
    print('Средний уровень безопасности')
elif 60 < security_is.output['Security'] < 80:
    print('Хороший уровень безопасности')
elif 80 < security_is.output['Security'] < 100:
    print('Высокий уровень безопасности')

print('Уровень угрозы составляет ' + str('%.2f' % (100 - security_is.output['Security'])) + '%')
security.view(sim=security_is)
plt.show()
